#include "watercell.h"

WaterCell::WaterCell():
    Cell(Water)
{

}

WaterCell::~WaterCell()
{

}
void WaterCell::iterate(int x, int y)
{
    if(!SandGame::getCellInstance(x,y)->m_updated && SandGame::getCellInstance(x,y)->m_type == Water)
    {
        if((x+1) >= SandGame::getRowCount())
        {
            SandGame::getCellInstance(x,y)->m_updated = true;
        }
        else
        {
            //Check the bottom cell, if it's not sand, it will fall down
            if(SandGame::checkYBoundaries(x+1) && SandGame::getCellInstance(x+1,y)->m_type == Empty)
            {
                SandGame::getCellInstance(x+1,y) = &Cell::createCell(Water);
                SandGame::getCellInstance(x+1,y)->m_updated = true;
                delete SandGame::getCellInstance(x,y);
                SandGame::assignEmptyCell(x,y);
            }
            else
            {
                randomFall(x, y, 0);
            }
        }
    }
}

Cell::Species WaterCell::returnType()
{
    return m_type;
}

Cell &WaterCell::getInstance()
{
    return *m_instance;
}

void WaterCell::randomFall(int x, int y, int randomNum)
{
    switch(randomNum)
    {
    case 0:
        if(SandGame::checkBoundaries(x+1, y+1) && SandGame::getCellInstance(x+1,y+1)->m_type == Empty)
        {
            SandGame::getCellInstance(x+1,y+1) = &Cell::createCell(Water);
            SandGame::getCellInstance(x+1,y+1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 1:
        if(SandGame::checkBoundaries(x+1, y-1) && SandGame::getCellInstance(x+1,y-1)->m_type == Empty)
        {
            SandGame::getCellInstance(x+1,y-1) = &Cell::createCell(Water);
            SandGame::getCellInstance(x+1,y-1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 2:
        if(SandGame::checkBoundaries(x, y-1) && SandGame::getCellInstance(x,y-1)->m_type == Empty)
        {
            SandGame::getCellInstance(x,y-1) = &Cell::createCell(Water);
            SandGame::getCellInstance(x,y-1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 3:
        if(SandGame::checkBoundaries(x, y+1) && SandGame::getCellInstance(x,y+1)->m_type == Empty)
        {
            SandGame::getCellInstance(x,y+1) = &Cell::createCell(Water);
            SandGame::getCellInstance(x,y+1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 4:
        SandGame::getCellInstance(x,y)->m_updated = true;
        break;
    default:
        break;
    }
}
