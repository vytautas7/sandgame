#ifndef ICECELL_H
#define ICECELL_H
#include "cell.h"
#include "sandgame.h"

class IceCell : public Cell
{
public:
    IceCell();
    ~IceCell();
    void iterate(int x, int y) override;
    Species returnType() override;
    Cell& getInstance() override;
};

#endif // ICECELL_H
