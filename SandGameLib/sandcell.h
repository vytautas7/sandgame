#ifndef SANDCELL_H
#define SANDCELL_H
#include "cell.h"
#include "sandgame.h"

class SandCell: public Cell
{
public:
    SandCell();
    ~SandCell();
    void iterate(int x, int y) override;
    Species returnType() override;
    Cell& getInstance() override;

private:
    void randomFall(int i, int j, int randomDir);
};

#endif // SANDCELL_H
