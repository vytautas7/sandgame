#include "sandcell.h"

SandCell::SandCell():
    Cell(Sand)
{

}

SandCell::~SandCell()
{

}

Cell::Species SandCell::returnType()
{
    return m_type;
}

Cell &SandCell::getInstance()
{
    return *m_instance;
}

void SandCell::iterate(int x, int y)
{
    if(!SandGame::getCellInstance(x,y)->m_updated && SandGame::getCellInstance(x,y)->m_type == Sand)
    {
        if((x+1) >= SandGame::getRowCount())
        {
            SandGame::getCellInstance(x,y)->m_updated = true;
        }
        else
        {
            switch(static_cast<int>(SandGame::getCellInstance(x+1,y)->m_type))
            {
            case Empty:
                SandGame::getCellInstance(x+1,y) = &Cell::createCell(Sand);
                SandGame::getCellInstance(x+1,y)->m_updated = true;
                delete SandGame::getCellInstance(x,y);
                SandGame::assignEmptyCell(x,y);
                break;
            case Water:
                //replace positions
                delete SandGame::getCellInstance(x+1,y);
                SandGame::getCellInstance(x+1,y) = &Cell::createCell(Sand);
                SandGame::getCellInstance(x+1,y)->m_updated = true;
                delete SandGame::getCellInstance(x,y);
                SandGame::getCellInstance(x,y) = &Cell::createCell(Water);
                SandGame::getCellInstance(x,y)->m_updated = true;
                break;
            case Wall:
                SandGame::getCellInstance(x,y)->m_updated = true;
                break;
            default:
                randomFall(x, y, 0);
            }
        }
    }
}

void SandCell::randomFall(int x, int y, int randomNum)
{
    switch(randomNum)
    {
    case 0:
        if(SandGame::checkBoundaries(x+1, y+1) && SandGame::getCellInstance(x+1,y+1)->m_type == Empty)
        {
            SandGame::getCellInstance(x+1,y+1) = &Cell::createCell(Sand);
            SandGame::getCellInstance(x+1,y+1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 1:
        if(SandGame::checkBoundaries(x+1, y-1) && SandGame::getCellInstance(x+1,y-1)->m_type == Empty)
        {
            SandGame::getCellInstance(x+1,y-1) = &Cell::createCell(Sand);
            SandGame::getCellInstance(x+1,y-1)->m_updated = true;
            delete SandGame::getCellInstance(x,y);
            SandGame::assignEmptyCell(x,y);
        }
        else
        {
            randomFall(x, y, ++randomNum);
        }
        break;
    case 2:
        SandGame::getCellInstance(x,y)->m_updated = true;
        break;
    default:
        break;
    }
}
