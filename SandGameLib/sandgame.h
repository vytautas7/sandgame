#ifndef SANDGAME_H
#define SANDGAME_H

#include <QObject>
#include <qqml.h>
#include <QAbstractTableModel>
#include <vector>
#include <cell.h>

class SandGame : public QAbstractTableModel
{
    Q_OBJECT
 public:
    explicit SandGame(QObject *parent = nullptr);
    QHash<int, QByteArray> roleNames() const override;
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value,int role = Qt::EditRole) override;
    Q_INVOKABLE virtual void iterate();
    void clearUpdated();

    inline static int getRowCount() { return Heigth; }
    inline static int getColumnCount() { return Length; }
    inline static Cell* &getCellInstance(int i, int j)
    {;
        return m_cellVector[i][j];
    }
    inline static void assignEmptyCell(int i, int j) { m_cellVector[i][j] = &m_emptyCell; }
    inline static bool checkXBoundaries(int coord) { if(coord >= getColumnCount() || coord < 0) return false; return true; }
    inline static bool checkYBoundaries(int coord) { if(coord >= getRowCount() || coord < 0) return false; return true; }
    inline static bool checkBoundaries(int x, int y) { return checkXBoundaries(x) && checkYBoundaries(y); }

private:
    enum Roles {
        CellRole
    };
    static Cell& m_emptyCell;
    static CellGrid m_cellVector;

    static const int Heigth;
    static const int Length;
};

#endif // SANDGAME_H
