#include "cell.h"
#include <stdio.h>
#include "emptycell.h"
#include "sandcell.h"
#include "watercell.h"
#include "wallcell.h"
#include "icecell.h"

Cell& Cell::createCell(Cell::Species type)
{
    int intType = static_cast<int>(type);
    switch(intType)
    {
    case Cell::Species::Sand:
    {
        auto temp = new SandCell();
        return temp->getInstance();
        break;
    }
    case Cell::Species::Water:
    {
        auto temp = new WaterCell();
        return temp->getInstance();
        break;
    }

    case Cell::Species::Wall:
    {
        auto temp = new WallCell();
        return temp->getInstance();
        break;
    }
    case Cell::Species::Ice:
    {
        auto temp = new IceCell();
        return temp->getInstance();
        break;
    }

    default:
    {
        auto temp = new EmptyCell();
        return temp->getInstance();
        break;
    }
    }
}

Cell::Cell(Species type):
    m_type(type),
    m_updated(false)
{
    m_instance = this;
}

Cell::~Cell()
{

}
