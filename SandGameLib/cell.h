#ifndef CELL_H
#define CELL_H
#include <cstdint>
#include <vector>

class Cell;
typedef std::vector<std::vector<Cell*>> CellGrid;

class Cell
{
public:
    enum Species
    {
        Empty = 0,
        Sand = 1,
        Water = 2,
        Wall = 3,
        Ice = 4
    };
    static Cell& createCell(Species type);
    static void shutdown();

    Cell(Species type);
    virtual ~Cell();

    virtual void iterate(int x, int y) = 0;
    virtual Species returnType() = 0;
    virtual Cell& getInstance() = 0;

    Species m_type;
    Cell* m_instance;
    bool m_updated;

};


#endif // CELL_H
