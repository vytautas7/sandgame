#ifndef WATERCELL_H
#define WATERCELL_H
#include "cell.h"
#include "sandgame.h"

class WaterCell: public Cell
{
public:
    WaterCell();
    ~WaterCell();
    void iterate(int x, int y) override;
    virtual Species returnType() override;
    Cell& getInstance() override;

private:
    void randomFall(int i, int j, int randomDir);
};

#endif // WATERCELL_H
