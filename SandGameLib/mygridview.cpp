#include "mygridview.h"
#include "sandgame.h"
#include <sstream>
#include <iostream>
#include <map>

MyGridView::MyGridView(QQuickItem *parent) : QQuickPaintedItem(parent),
    m_penSize(4),
    m_cellType(0)
{
    setAcceptedMouseButtons(Qt::AllButtons);
}

void MyGridView::paint(QPainter *painter)
{
    updateScale();
    const auto bg = QColor(200, 200, 200);

    auto tempColor = bg;
    std::map<Cell::Species, std::vector<QPoint>> pointMap;
    pointMap[Cell::Sand] = std::vector<QPoint>();
    pointMap[Cell::Water] = std::vector<QPoint>();
    pointMap[Cell::Wall] = std::vector<QPoint>();
    pointMap[Cell::Ice] = std::vector<QPoint>();
    pointMap[Cell::Empty] = std::vector<QPoint>();

    static std::map<Cell::Species, QColor> colorMap;
    colorMap[Cell::Sand] = QColor::fromCmyk(0, 0, 255, 0);
    colorMap[Cell::Water] = QColor(0, 0, 255);
    colorMap[Cell::Wall] = QColor(0, 0, 0);
    colorMap[Cell::Ice] = QColor::fromCmyk(255, 0, 0, 0);
    colorMap[Cell::Empty] = QColor(255, 255, 255);


    for(int i=0; i<m_model->rowCount(); i++)
    {
       for(int j=0; j<m_model->columnCount(); j++)
       {
           auto idx = m_model->index(i, j);
           auto value = m_model->data(idx);

           auto cellType = static_cast<Cell::Species>(value.toInt());
           if(cellType != Cell::Empty)
               pointMap[cellType].push_back(modelToView(i, j));
       }
    }

    for(auto pair : pointMap)
    {
        painter->setPen(QPen(QBrush(colorMap[pair.first]), m_penSize, Qt::PenStyle::SolidLine, Qt::PenCapStyle::RoundCap, Qt::PenJoinStyle::RoundJoin));
        painter->setBrush(Qt::BrushStyle::SolidPattern);
        for(auto point : pair.second) {
            painter->drawPoint(point);
        }
    }
}

QPoint MyGridView::modelToView(int row, int column)
{
    return QPoint(round(column*m_scaleX), round(row*m_scaleY));
}

std::pair<int, int> MyGridView::viewToModel(int x, int y)
{
    return std::pair<int, int>(floor(x/m_scaleX), floor(y/m_scaleY));
}

void MyGridView::mouseMoveEvent(QMouseEvent *event)
{
    event->accept();

    QPointF pos = event->localPos();
    //Check if we dont have negative coordinates

    auto coords = viewToModel(pos.x(), pos.y());

    for(int i=0; i<5; i++) {
        for(int j=0; j<5; j++) {
            bool negativeCoord = (coords.first+i < 0 || coords.second+j < 0);
            if(coords.first+i < SandGame::getRowCount() && coords.second+j < SandGame::getColumnCount() && !negativeCoord) {
                auto idx = m_model->index(coords.second+j, coords.first+i);
                m_model->setData(idx, QVariant(m_cellType));
            }
        }
    }

    update();
}
void MyGridView::mousePressEvent(QMouseEvent *event)
{
    event->accept();
}

void MyGridView::updateScale()
{
    m_scaleX = width()/m_model->columnCount();
    m_scaleY = height()/m_model->rowCount();
}
