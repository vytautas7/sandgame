#include "emptycell.h"

EmptyCell::EmptyCell():
    Cell(Empty)
{

}

EmptyCell::~EmptyCell()
{

}

void EmptyCell::iterate(int x, int y)
{

}

Cell::Species EmptyCell::returnType()
{
    return m_type;
}

Cell &EmptyCell::getInstance()
{
    return *m_instance;
}
