#include "sandgame.h"
#include <tgmath.h>

const int SandGame::Heigth = 300;
const int SandGame::Length = 300;
Cell& SandGame::m_emptyCell = Cell::createCell(Cell::Empty);
CellGrid SandGame::m_cellVector( SandGame::getRowCount() , std::vector<Cell*> (SandGame::getColumnCount(), &m_emptyCell));

SandGame::SandGame(QObject *parent) :
    QAbstractTableModel(parent)
{
}

QHash<int, QByteArray> SandGame::roleNames() const
{
    return
    {
        { CellRole, "value" }
    };
}

int SandGame::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return Heigth;
}

int SandGame::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;

    return Length;
}

QVariant SandGame::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || role != CellRole)
        return QVariant();

    return QVariant(m_cellVector[index.row()][index.column()]->returnType());
}

bool SandGame::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) == value)
        return false;

    if(value.toInt() == 0)
    {
        m_cellVector[index.row()][index.column()] = &m_emptyCell;
    }
    else if ( m_cellVector[index.row()][index.column()]->m_type == Cell::Empty)
    {
        m_cellVector[index.row()][index.column()] = &Cell::createCell(Cell::Species(value.toInt()));
    }
    emit dataChanged(index, index, {role});

    return true;
}

void SandGame::iterate()
{
    clearUpdated();
#pragma omp for
    for(int i = 0; i< Heigth; i++)
    {
        for (int j = 0; j < Length; j++)
        {
            m_cellVector[i][j]->iterate(i, j);
        }
    }
    emit dataChanged(index(0, 0), index(Heigth - 1, Length - 1), {CellRole});
}

void SandGame::clearUpdated()
{
#pragma omp for
    for(int i = 0; i < Heigth; i++)
    {
        for(int j = 0; j < Length; j++)
        {
            m_cellVector[i][j]->m_updated = false;
        }
    }
}
