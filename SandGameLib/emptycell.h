#ifndef EMPTYCELL_H
#define EMPTYCELL_H
#include "cell.h"

class EmptyCell : public Cell
{
public:
    EmptyCell();
    ~EmptyCell();
    void iterate(int x, int y) override;
    Species returnType() override;
    Cell& getInstance() override;
};

#endif // EMPTYCELL_H
