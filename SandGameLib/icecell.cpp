#include "icecell.h"

IceCell::IceCell():
    Cell(Ice)
{

}

IceCell::~IceCell()
{

}

void IceCell::iterate(int x, int y)
{
    if(!SandGame::getCellInstance(x,y)->m_updated && SandGame::getCellInstance(x,y)->m_type == Ice)
    {
        //For ice we have to check all surrouding cells
        if(SandGame::checkXBoundaries(x+1))
        {
            switch(static_cast<int>(SandGame::getCellInstance(x+1,y)->m_type))
            {
            case Water:
                delete SandGame::getCellInstance(x+1,y);
                SandGame::getCellInstance(x+1,y) = &Cell::createCell(Ice);
                SandGame::getCellInstance(x+1,y)->m_updated = true;
            }
        }
        if(SandGame::checkXBoundaries(x-1))
        {
            switch(static_cast<int>(SandGame::getCellInstance(x-1,y)->m_type))
            {
            case Water:
                delete SandGame::getCellInstance(x-1,y);
                SandGame::getCellInstance(x-1,y) = &Cell::createCell(Ice);
                SandGame::getCellInstance(x-1,y)->m_updated = true;
            }
        }
        if(SandGame::checkYBoundaries(y-1))
        {
            switch(static_cast<int>(SandGame::getCellInstance(x,y-1)->m_type))
            {
            case Water:
                delete SandGame::getCellInstance(x,y-1);
                SandGame::getCellInstance(x,y-1) = &Cell::createCell(Ice);
                SandGame::getCellInstance(x,y-1)->m_updated = true;
            }
        }
        if(SandGame::checkYBoundaries(y+1))
        {
            switch(static_cast<int>(SandGame::getCellInstance(x,y+1)->m_type))
            {
            case Water:
                delete SandGame::getCellInstance(x,y+1);
                SandGame::getCellInstance(x,y+1) = &Cell::createCell(Ice);
                SandGame::getCellInstance(x,y+1)->m_updated = true;
            }
        }
    }
}

Cell::Species IceCell::returnType()
{
    return m_type;
}

Cell &IceCell::getInstance()
{
    return *m_instance;
}
