QT += quick

TEMPLATE = lib
DEFINES += SANDGAMELIB_LIBRARY

CONFIG += staticlib c++11 console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    sandgamelib.cpp \
    cell.cpp \
    emptycell.cpp \
    icecell.cpp \
    mygridview.cpp \
    sandcell.cpp \
    sandgame.cpp \
    wallcell.cpp \
    watercell.cpp

HEADERS += \
    SandGameLib_global.h \
    sandgamelib.h \
    cell.h \
    emptycell.h \
    icecell.h \
    mygridview.h \
    sandcell.h \
    sandgame.h \
    wallcell.h \
    watercell.h

# Default rules for deployment.
unix {
    target.path = /usr/lib
}
!isEmpty(target.path): INSTALLS += target
