#ifndef WALLCELL_H
#define WALLCELL_H
#include "cell.h"
#include "sandgame.h"

class WallCell : public Cell
{
public:
    WallCell();
    ~WallCell();
    void iterate(int x, int y) override;
    Species returnType() override;
    Cell& getInstance() override;
};

#endif // WALLCELL_H
