#ifndef SANDGAMELIB_GLOBAL_H
#define SANDGAMELIB_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SANDGAMELIB_LIBRARY)
#  define SANDGAMELIB_EXPORT Q_DECL_EXPORT
#else
#  define SANDGAMELIB_EXPORT Q_DECL_IMPORT
#endif

#endif // SANDGAMELIB_GLOBAL_H
