#ifndef MYGRIDVIEW_H
#define MYGRIDVIEW_H

#include <QtQuick>
#include "sandgame.h"
#include <memory>

class MyGridView : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(SandGame* gameModel READ getGameModel WRITE setGameModel NOTIFY gameModelChanged)
    Q_PROPERTY(int cellType READ getCellType WRITE setCellType NOTIFY cellTypeChanged);

public:
    MyGridView(QQuickItem *parent = nullptr);
    Q_INVOKABLE void paint(QPainter *painter) override;

    void setGameModel(SandGame *gameModel) { m_model.reset(gameModel);};
    SandGame* getGameModel() { return m_model.get(); };
    void setCellType(int type) { m_cellType = type; emit cellTypeChanged(); }
    int getCellType() { return m_cellType; }

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;


private:
    std::unique_ptr<SandGame> m_model;
    QPoint modelToView(int row, int column);
    std::pair<int, int> viewToModel(int x, int y);
    int m_penSize;
    float m_scaleX;
    float m_scaleY;
    int m_cellType;
    void updateScale();
    QMouseEvent* m_mouseEvent;

signals:
    void gameModelChanged();
    void cellTypeChanged();
};

#endif // MYGRIDVIEW_H
