# sandgame

Let's create our own sandgame
- Example: https://sandspiel.club/
- Simulation model
  - A grid of particles X * Y
  - Some general principles
    - Only one particle per cell
    - Same type of particles are able to stack on top of each other
    - Different types of particles may interact: both annihilate, turn one into other, etc.
  - Particles have different types
    - Empty particle is doing nothing
    - Wall particle attaches to the background and is not affected by gravity
    - Sand particle is affected by gravity, falls down when there is nothing below. Falls to side if pressed from above and there is place to the side.
    - Water particle is affected by gravity, is able to put out fires, water plants or get turned into ice. Falls down when ther is nothing below. Falls to side if pressed from above and ther is place.
    - Stone particle is similar to sand, but different color.
    - Ice particle can freeze water, is not affected by gravity. When burned by fire turns back into water.
    - Gas particle is not attached to the background, but is able to float by random Brownian motion.
    - Cloner particle replicates particles that it touches, itself is not affected by gravity.
    - Wood particle is not affected by gravity. Can turn on fire if touches fire.
    - Plant particle is able to grow if it touches water
    - Fire particle can turn things on fire (wood, plants). Can turn ice into water. Is extinguished by water. In general spreads up, but when touching.
    - Acid particle - is affected by gravity. When touches other particles they both annihilate.
- Visual renderer
    - Transforms model state into visual grid of pixels.
- Mouse input
    - Upon clicking on the grid - creates a particle at that position if the position is empty.
- GUI controls
    - QT UI buttons.
    - Different buttons modify the action of mouse click events
