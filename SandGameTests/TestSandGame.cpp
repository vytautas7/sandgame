#include <sandgame.h>
#include <tst_testcasebase.h>
#include <memory>
#include <iostream>

class TestSandGame : public ::testing::Test
{
public:
    TestSandGame() : m_itemUnderTest(nullptr)
    {

    }

    void SetUp() {

        m_itemUnderTest = std::make_unique<SandGame>();
    }

    void TearDown() {

    }

    void setCell(int x, int y, Cell::Species species)
    {
        auto idx = m_itemUnderTest->index(y, x);
        m_itemUnderTest->setData(idx, species);
    }

    Cell::Species getCell(int x, int y)
    {
        auto idx = m_itemUnderTest->index(y, x);
        auto cell = m_itemUnderTest->data(idx);

        return static_cast<Cell::Species>(cell.toInt());
    }

    void assertArea(int x1, int y1, int x2, int y2, Cell::Species species)
    {
        for(int i=x1; i<=x2; i++)
        {
            for(int j=y1; j<=y2; j++)
            {
                std::cout << i << ", " << j << ": " << getCell(i, j);
                EXPECT_EQ(getCell(i, j), species);
            }
        }
    }

    void setCellArea(int x1, int y1, int x2, int y2, Cell::Species species)
    {
        for(int i=x1; i<=x2; i++)
        {
            for(int j=y1; j<=y2; j++)
            {
                setCell(i, j, species);
            }
        }
    }

    std::unique_ptr<SandGame> m_itemUnderTest;
};

TEST_F(TestSandGame, testGridSize)
{
    EXPECT_EQ(m_itemUnderTest->columnCount(), m_itemUnderTest->rowCount());
}

TEST_F(TestSandGame, testSandSteadyDrop)
{
    assertArea(0, 0, 3, 2, Cell::Empty);

    setCellArea(0, 0, 3, 0, Cell::Sand);

    m_itemUnderTest->iterate();
    assertArea(0, 0, 3, 0, Cell::Empty);
    assertArea(0, 1, 3, 1, Cell::Sand);
    assertArea(0, 2, 3, 2, Cell::Empty);
    assertArea(0, 0, 3, 0, Cell::Water);
}
