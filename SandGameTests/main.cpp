#include "tst_testcasebase.h"
#include <gtest/gtest.h>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <sandgame.h>
#include <mygridview.h>

int main(int argc, char *argv[])
{
    /*
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);
    qmlRegisterType<SandGame>("SandGame", 1, 0, "SandGame");
    qmlRegisterType<MyGridView>("MyGridView", 1, 0, "MyGridView");

    qDebug("Starting application test");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);
    */

    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
