GOOGLETEST_DIR = ../vendor/googletest/
include(gtest_dependency.pri)

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG += thread
QT += quick
#CONFIG -= qt

HEADERS += \
        MockSandGame.h \
        itgrtestbase.h \
        tst_testcasebase.h

SOURCES += \
        TestCell.cpp \
        TestSandGame.cpp \
        itgrtestbase.cpp \
        main.cpp

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../SandGameLib/release/ -lSandGameLib
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../SandGameLib/debug/ -lSandGameLib
else:unix: LIBS += -L$$OUT_PWD/../SandGameLib/ -lSandGameLib

INCLUDEPATH += $$PWD/../SandGameLib
DEPENDPATH += $$PWD/../SandGameLib

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SandGameLib/release/libSandGameLib.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SandGameLib/debug/libSandGameLib.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SandGameLib/release/SandGameLib.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$OUT_PWD/../SandGameLib/debug/SandGameLib.lib
else:unix: PRE_TARGETDEPS += $$OUT_PWD/../SandGameLib/libSandGameLib.a
