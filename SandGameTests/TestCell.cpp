#include <cell.h>
#include <tst_testcasebase.h>
#include <memory>

TEST(TestCell, TestCreateCell)
{
    std::vector<Cell::Species> types;
    types.push_back(Cell::Empty);
    types.push_back(Cell::Sand);
    types.push_back(Cell::Wall);
    types.push_back(Cell::Water);
    types.push_back(Cell::Ice);

    std::unique_ptr<Cell> cellptr;

    for(auto type : types) {
        cellptr.reset(&Cell::createCell(Cell::Ice));
        EXPECT_EQ(Cell::Ice, cellptr->returnType());
    }
}
