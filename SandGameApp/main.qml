import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import SandGame 1.0
import MyGridView 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 800
    height: 600
    title: qsTr("Sandgame")

    MyGridView {
        id: gridView

        anchors.fill: parent

        gameModel: SandGame {
            id: sandgame
            onDataChanged: gridView.update()
        }
    }

    footer: Rectangle {
        //signal iterate

        id: footer
        height: 50
        color: "#F3F3F4"

        RowLayout {
            id: cellPicker
            anchors.centerIn: parent

            Button {
                id: emptyButton
                text: qsTr("Empty")
                onClicked:
                {
                    gridView.cellType = 0
                }
                background: Rectangle
                {
                    color: gridView.cellType == 0 ? "#ccc" : "#eee"

                }

            }
            Button {
                id: sandButton
                text: qsTr("Sand")
                onClicked:
                {
                    gridView.cellType = 1
                }
                background: Rectangle
                {
                    color: gridView.cellType == 1 ? "#ccc" : "#eee"

                }
            }
            Button {
                id: waterButton
                text: qsTr("Water")
                onClicked:
                {
                    gridView.cellType = 2
                }
                background: Rectangle
                {
                    color: gridView.cellType == 2 ? "#ccc" : "#eee"

                }
            }
            Button {
                id: wallButton
                text: qsTr("Wall")
                onClicked:
                {
                    gridView.cellType = 3
                }
                background: Rectangle
                {
                    color: gridView.cellType == 3 ? "#ccc" : "#eee"

                }
            }
            Button {
                id: iceButton
                text: qsTr("Ice")
                onClicked:
                {
                    gridView.cellType = 4
                }
                background: Rectangle
                {
                    color: gridView.cellType == 4 ? "#ccc" : "#eee"

                }
            }
            Button {
                text: qsTr("Next")
                onClicked: sandgame.iterate()
            }

            Slider {
                id: slider
                from: 0
                to: 1
                value: 1
            }

            Button {
                text: timer.running ? "❙❙" : "▶️"
                onClicked: {
                    timer.running = !timer.running
                }
            }

        }

        Timer {
            id: timer
            interval: 1000 - (999 * slider.value)
            running: false
            repeat: true
            onTriggered: sandgame.iterate()
        }
    }
}
